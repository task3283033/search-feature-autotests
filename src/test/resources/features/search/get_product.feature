Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: As a customer I want to search for a product available
    When customer calls search endpoint for <product>
    Then customer sees status code is 200
    And customer sees the <results> displayed for product

    Examples:
      | product | results              |
      | orange  | orange               |
      | apple   | apple                |
      | pasta   | Unox Good Pasta kaas |
      | cola    | Coca-Cola Blik       |


  Scenario: As a customer I should not see results if search category invalid
    When customer calls search endpoint for car
    Then customer sees status code is 404
    And customer sees Error field is true
