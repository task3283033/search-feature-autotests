package starter.com.screenplay.restapi.basics;

import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;

public class BaseResponse {
    public <T> T getBodyAsObject(Response response, Class<T> targetType) {
        return response.body().as(targetType, ObjectMapperType.GSON);
    }
}
