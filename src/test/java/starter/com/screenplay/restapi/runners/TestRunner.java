package starter.com.screenplay.restapi.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/search",
        glue = {"starter.com.screenplay.restapi.stepdefinitions"}
)
public class TestRunner {}
