package starter.com.screenplay.restapi.question;

import net.serenitybdd.screenplay.Question;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class ResponseStatusCode {
    @Step("Customer sees response status code is {0}")
    public static Question<Boolean> is(int statusCode) {
        return actor -> lastResponse().statusCode() == statusCode;
    }
}
