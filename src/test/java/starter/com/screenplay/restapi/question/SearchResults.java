package starter.com.screenplay.restapi.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.thucydides.core.annotations.Step;
import starter.com.screenplay.restapi.basics.BaseResponse;
import starter.com.screenplay.restapi.model.response.search.ProductSearchResult;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class SearchResults extends BaseResponse implements Question<Boolean> {

    private final String expectedResult;

    public SearchResults(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        List<ProductSearchResult> foundProducts = Arrays.asList(getBodyAsObject(lastResponse(), ProductSearchResult[].class));

        return getProductTitlesFrom(foundProducts).contains(expectedResult);
    }

    @Step("Custer ensures that title {0} is in search results")
    public static SearchResults contains(String expectedResult) {
        return new SearchResults(expectedResult);
    }

    private List<String> getProductTitlesFrom(List<ProductSearchResult> searchResults) {
        return searchResults.stream()
                .map(ProductSearchResult::getTitle)
                .collect(Collectors.toList());
    }
}
