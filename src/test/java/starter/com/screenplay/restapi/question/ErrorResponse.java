package starter.com.screenplay.restapi.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import starter.com.screenplay.restapi.basics.BaseResponse;
import starter.com.screenplay.restapi.model.response.error.ProductNotFoundResponse;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class ErrorResponse extends BaseResponse implements Question<Boolean> {
    private boolean isError;

    public ErrorResponse(boolean isError) {
        this.isError = isError;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        ProductNotFoundResponse productNotFoundResponse = getBodyAsObject(lastResponse(), ProductNotFoundResponse.class);

        return productNotFoundResponse.getDetail().isError();
    }

    public static ErrorResponse errorFieldIs(boolean isError) {
        return new ErrorResponse(isError);
    }
}
