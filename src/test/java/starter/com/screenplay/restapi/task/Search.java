package starter.com.screenplay.restapi.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Search implements Task {
    private final String product;

    public Search(String product) {
        this.product = product;
    }

    @Override
    public <T extends Actor> void performAs(T theActor) {
        theActor.attemptsTo(
                Get.resource(product)
        );
    }

    @Step("Customer search for {0}")
    public static Search aProduct(String product) {
        return instrumented(Search.class, product);
    }
}
