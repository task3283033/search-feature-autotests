package starter.com.screenplay.restapi.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import starter.com.screenplay.restapi.constants.Endpoints;
import starter.com.screenplay.restapi.question.ErrorResponse;
import starter.com.screenplay.restapi.question.ResponseStatusCode;
import starter.com.screenplay.restapi.question.SearchResults;
import starter.com.screenplay.restapi.task.Search;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SearchStepDefinitions {
    private Actor customer;

    @Before
    public void setCustomer() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("customer").whoCan(CallAnApi.at(Endpoints.SEARCH_ENDPOINT));
    }


    @When("^customer calls search endpoint for (.*)$")
    public void customerCallsSearchEndpointForProduct(String product) {
        theActorInTheSpotlight().attemptsTo(Search.aProduct(product));
    }

    @Then("customer sees status code is {int}")
    public void customerSeesStatusCodeIs(int statusCode) {
        theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("Last response status code is " + statusCode, ResponseStatusCode.is(statusCode)));
    }

    @And("^customer sees the (.*) displayed for product$")
    public void customerSeesTheResultsDisplayedForProductInNonEmptyCategory(String expectedProduct) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(SearchResults.contains(expectedProduct)));
    }

    @And("^customer sees Error field is (.*)$")
    public void customerSeesErrorField(boolean expectedToBeTrue) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ErrorResponse.errorFieldIs(expectedToBeTrue)));
    }
}
