package starter.com.screenplay.restapi.model.response.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSearchResult {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private double price;
    private String unit;
    private boolean isPromo;
    private Object promoDetails;
    private String image;
}
