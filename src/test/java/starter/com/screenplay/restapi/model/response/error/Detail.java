package starter.com.screenplay.restapi.model.response.error;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Detail {
    private boolean error;
    private String message;
    private String requested_item;
    private String served_by;
}
