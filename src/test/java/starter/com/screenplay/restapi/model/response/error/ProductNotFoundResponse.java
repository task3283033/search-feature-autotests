package starter.com.screenplay.restapi.model.response.error;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductNotFoundResponse {
    private Detail detail;
}
