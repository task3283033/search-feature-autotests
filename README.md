# Automated testing framework for Rest API testing

This framework dedicated for test coverage the services with Rest API tests

## Installation process 
To get code of this framework clone this project from the repository

## How to run tests
### Locally
Once project is cloned to the local machine to run it use command 'mvn clean verify' in terminal.
Also, it can be launch directly from IDE by clicking on 'Play' button in class(es) that located in 'runner' package
### On GitLab CI/CD
This project contains gitlab-ci.yml file. To launch tests in pipeline
required the next:<br />
1. Download and configure runner.<br/>
<span style="color:red">
Important
</span>
In field tags specify tag **'shalllocal'**<br/>
2. Run pipeline by clicking button 'Run pipeline' on Pipeline page.<br/>

### Adding new tests
To add new test:
1. Create file with extension .feature in folder 'features'. In this file specify test scenario.<br/>
2. Create class with step definitions for each scenario step.
3. Since this framework uses 'Screenplay' pattern, please follow rules of it.<br/>
   Implement tasks in package 'tasks' or use already defined
   Implement questions in package 'questions' or use already defined
4. Pojo representation of response body located in package 'model'

### What's been refactored
1. Removed all entities related to Gradle builder (as frameworks builder is Maven)
2. post_product.feature renamed as get_product.feature (search feature more relates to method HTTP method GET instead of POST)
3. Original scenario divided into positive scenario outline and negative scenario. (it is more convenient in terms of reading)
4. Change scenario steps description to more suitable
5. Implemented new step definitions in class SearchStepDefinition (according to new step description)
6. Removed class CarAPI (it is no longer needed for the new implementation)
7. Introduced new classes (for implementation Screenplay pattern and data representation)
8. Add new dependencies to pom.xml (for implementation of 'Screenplay' for Rest API testing)

### Html report
Html report could be found for each test run on page 'Artifacts'

